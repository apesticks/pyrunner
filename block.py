from pygame import *

class Block(sprite.Sprite):
    def __init__(self, path=None, width=32, height=32):
        super(Block, self).__init__()
        self.width = width;
        self.height = height

        if path:
            self.image = image.load(path)
            self.image = transform.scale(self.image, (self.width, self.height))
        self.rect = self.image.get_rect()

    def set_position(self, x, y):
        self.rect.x = x
        self.rect.y = y
