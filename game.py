import pygame
from block import Block
from level_generator import LevelGenerator
from pygame import *

class Game(object):
    def __init__(self, screen_size=(800,600)):
        pygame.init()
        self.running = True
        self.fps = pygame.time.Clock()

        #sets the screen to apear in same position everytime
        self.window_position = 1000, 400
        self.screen_size = screen_size
        self.screen = pygame.display.set_mode(self.screen_size ,0 , 32)

        self.wall_group = sprite.Group()

        self.level1 = LevelGenerator("map.txt")
        for block in self.level1.blocks:
            self.wall_group.add(block)

    def run(self):
        """handles game loop"""
        while self.running:
            self.fps.tick(60)
            self.events()
            self.screen.fill((0,0,0))
            self.wall_group.draw(self.screen)
            pygame.display.update()

    def events(self):
        """handles game events(keyboard)"""
        for event in pygame.event.get():
            if event.type == QUIT:
                self.running = False
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    self.running = False
                if event.key == K_RIGHT:
                    new_block_list = self.level1.generate_new_tile()
                    for block in new_block_list:
                        self.wall_group.add(block)
