from block import Block
from pygame import *
from registry import *

import random

class LevelGenerator():
    def __init__(self, map_path=None):
        self.map_path = map_path
        self.draw_length = 3
        self.blocks = []
        self.tile_symbols = "@#*"
        self.start_list = [
            "##",
            "@#@",
            "@",
            "**",
        ]
        self.image_dict = {
                    "#": "red_brick.jpg",
                    "@": "blue_brick.jpg",
                    "*": "green_brick.jpg",
                }
        self.generate_map_dynamically()

    def generate_map_from_txt(self):
        with open(self.map_path, "r") as map_file:
            for row, line in enumerate(map_file):
                for col, char in enumerate(line):
                    try:
                        block = Block(self.image_dict[char])
                        block.set_position((width/2) + (col * block.width), (height/2) + (row * block.width))
                        self.blocks.append(block)
                    except:
                        pass

    def generate_map_dynamically(self):
        for row, line in enumerate(self.start_list):
            for col, char in enumerate(line):
                try:
                    block = Block(self.image_dict[char])
                    block.set_position((width/2) + (col * block.width), (height/2) + (row * block.width))
                    self.blocks.append(block)
                except:
                    pass

    def generate_new_tile(self):
        random_choice = random.choice(self.tile_symbols)
        temp = []

        for index, line in enumerate(self.start_list):
            random_choice = random.choice(self.tile_symbols)
            self.start_list[index] += random_choice
            block = Block(self.image_dict[random_choice])
            block.set_position((width/2) + ((len(self.start_list[index])-1) * block.width), (height/2) + (index * block.width))
            self.blocks.append(block)
            temp.append(block)

        return temp
